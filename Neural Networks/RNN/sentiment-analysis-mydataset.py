import sys
import sklearn
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.model_selection import train_test_split
from keras.preprocessing import sequence
from keras import Sequential
from keras.layers import Embedding, LSTM, Dense, Dropout
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences





train_files = ["sentiment labelled sentences/amazon_cells_labelled.txt","sentiment labelled sentences/yelp_labelled.txt","sentiment labelled sentences/imdb_labelled.txt"]

data , data_labels = [] , [] # Preprocessing Data(Separate Sentences from Classes)
for file in train_files: # For each training file
    with open(file) as f: # Loop through each the file
        for i in f:
            data.append(i[:-1]) # Put each sentence in data array
            if i[-2] == "1": # Identify all positive classes
                data_labels.append('1')
            else:
                data_labels.append('0') # Identify negative classes

# print(data)
vocublary = []
for word in data:
    # word = word[:-1]
    # print(sentence)
    for eachWord in word[:-1].split(" "):
        vocublary.append(eachWord)

vocabulary_size = 5000



vectorizer = CountVectorizer(
    analyzer = 'word',
    lowercase = False,
)
features = vectorizer.fit_transform(data)
features_nd = features.toarray()

# Convert raw frequency counts into TF-IDF values
tfidf_transformer = TfidfTransformer()
sent_tfidf = tfidf_transformer.fit_transform(features).toarray()

X_train, X_test, y_train, y_test  = train_test_split(
        features_nd,
        data_labels,
        train_size=0.80,
        random_state=1234)


print('Loaded dataset with {} training samples, {} test samples'.format(len(X_train), len(X_test)))

print(X_train)
max_words = 5888
# tk = Tokenizer()
# tk.fit_on_texts(data)
# index_list = tk.texts_to_sequences(data)
# X_train = sequence.pad_sequences(index_list, maxlen=max_words)

# X_train = sequence.pad_sequences(X_train, maxlen=max_words)
# X_test = sequence.pad_sequences(X_test, maxlen=max_words)


# Design an RNN model for sentiment analysis
embedding_size=32
model=Sequential()
model.add(Embedding(vocabulary_size, embedding_size, input_length=max_words))
model.add(LSTM(100))
model.add(Dense(1, activation='sigmoid'))
print(model.summary())

# Maximum review length and minimum review length.
# print('Maximum review length: {}'.format(len(max((X_train + X_test), key=len))))

# Train and evaluate our model
 # specifying the loss function and optimizer we want to use while training
model.compile(loss='binary_crossentropy',
             optimizer='adam',
             metrics=['accuracy'])

# print(model)
# Training process
batch_size = 64
num_epochs = 3
X_valid, y_valid = X_train[:batch_size], y_train[:batch_size]
X_train2, y_train2 = X_train[batch_size:], y_train[batch_size:]
# print(type(X_train))
#
model.fit(X_train, y_train, epochs=5, batch_size=32)
# model.fit(X_train2, y_train2, validation_data=(X_valid, y_valid), batch_size=batch_size, epochs=num_epochs)

# Testing model
# scores = model.evaluate(X_test, y_test, verbose=0)
# print('Test accuracy:', scores[1])
