# Emmanuel Jojoe Ainoo (MY NAIVE BAYES CLASSIFIER)
# Imports
import math
from collections import defaultdict
import string
import re
from collections import Counter
import sys

#Global Variables
vocub = {} #Variable to store Vocub
# Storing word_counts
word_counts = {
    "1": {},
    "0": {}
}

priors = {
    "1": 0.,
    "0": 0.
}
 # Storing logpriors of each class
logpriors = {
    "1":0.,
    "0":0.
}
# Storing loglikelihoods of words given class
loglikelihood = {}
loglikelihood["1"] = {}
loglikelihood["0"] = {}

# Function to transform all text into lower case
def LowerCase(sentence):
    return sentence.lower()

# Function to take out punctations from text
def RemovePunctuations(sentence):
    exclude = set(string.punctuation)
    return ''.join(ch for ch in sentence if ch not in exclude)

# Function to Normalize text by splitting texts based on lower cases and punctuations
def NormalizeText(sentence):
    sentence = LowerCase(sentence)
    sentence = RemovePunctuations(sentence)
    return re.split("\W+",sentence)

# Function to count the number of words in a given vocublary of words
def CountWords(words):
    wc = {}
    for word in words:
        wc[word] = wc.get(word, 0.0) + 1.0
    return wc

# Function to generate counts of words given each class and separate each word by class
def BuildCounts(file, vocub,word_counts,priors):
    document = []
    with open(file) as f:
        content = f.readlines()
        content = [x.strip() for x in content]
    for result in content:
        if "1" in result:
            category = "1"
        else:
            category = "0"
        document.append((category, result)) # Deriving array of each class and word
        priors[category] += 1
        words = NormalizeText(result)
        counts = CountWords(words)
        # Filling up vocublary and word_counts of each word and count
        for word, count in list(counts.items()):
            if word not in vocub:
                try:
                    vocub[word] = 0.0
                    word_counts[category][word] = 0.0
                except KeyError: # Catering for abnormal keys
                    continue
            try:
                vocub[word] += count
                word_counts[category][word] += count
            except KeyError:
                continue

# Function to train Naive Bayes Classifier
def TrainNaiveBayes(file, vocub, word_counts, priors):
    with open(file) as f:
        content = f.readlines()
        content = [x.strip() for x in content]
    for result in content:
        words = NormalizeText(result)
        counts = CountWords(words)
        import math

        prior_pos = (priors["1"] / sum(priors.values()))
        logpriors["1"] = prior_pos # Logprior for positive class

        prior_neg = (priors["0"] / sum(priors.values()))
        logpriors["0"] = prior_neg # Logprior for negative class


        log_prob_pos = 0.0
        log_prob_neg = 0.0

        for word, count in list(counts.items()):
            if word not in vocub or len(word) <= 3:
                continue

            p_word = vocub[word] / sum(vocub.values())

            p_w_given_neg = word_counts["0"].get(word, 0.0) / sum(word_counts["0"].values())
            p_w_given_pos = word_counts["1"].get(word, 0.0) / sum(word_counts["1"].values())


            if p_w_given_neg > 0:
                log_prob_neg += math.log(count * p_w_given_neg / p_word)
                loglikelihood["0"][word] = log_prob_neg

            if p_w_given_pos > 0:
                log_prob_pos += math.log(count * p_w_given_pos / p_word)
                loglikelihood["1"][word] = log_prob_pos


# Function to test Naive Bayes Classifier given trained logpriors and loglikelihoods
def TestNaiveBayes(file, vocub, logpriors, loglikelihood):
    results = open("results.txt", "w")
    with open(file) as f:
        content = f.readlines()
        content = [x.strip() for x in content]
    for result in content:
        words = NormalizeText(result)
        counts = CountWords(words)
        import math

        prior_pos = logpriors["1"]
        prior_neg = logpriors["0"]

        log_prob_pos = 0.0
        log_prob_neg = 0.0

        for word, count in list(counts.items()):
            if word not in vocub or len(word) <= 3:
                continue

            p_word = vocub[word] / sum(vocub.values())

            p_w_given_neg = word_counts["0"].get(word, 0.0) / sum(word_counts["0"].values())
            p_w_given_pos = word_counts["1"].get(word, 0.0) / sum(word_counts["1"].values())

            if p_w_given_neg > 0:
                log_prob_neg = loglikelihood["0"][word]

            if p_w_given_pos > 0:
                log_prob_pos = loglikelihood["1"][word]

        score_pos = math.exp(log_prob_pos + math.log(prior_pos))
        score_neg = math.exp(log_prob_neg + math.log(prior_neg))

        if(score_pos > score_neg):
            results.write("\n")
            results.write("1") # Write positive class for that sentence

        else:
            results.write("\n")
            results.write("0") # Write negative class for that sentence


if __name__ == "__main__":
    # Loop through folder and pick all files for training data
    TrainFiles = ["sentiment labelled sentences/amazon_cells_labelled.txt","sentiment labelled sentences/imdb_labelled.txt","sentiment labelled sentences/yelp_labelled.txt"]
    for each in TrainFiles:
        BuildCounts(each,vocub,word_counts, priors)
        TrainNaiveBayes(each, vocub, word_counts, priors)

    TestNaiveBayes(sys.argv[1], vocub, logpriors, loglikelihood)

else:
    print("Please Enter Filename")
