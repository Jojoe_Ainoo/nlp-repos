import numpy

# Function to Implement Minimum Edit Distance Algorithm
def MinEditDistance(source,target):
    n = len(source)
    m = len(target)

    D = numpy.zeros((n + 1, m + 1)) # Distance Matrix

#    Initialization: the zeroth row and column is the distance from the empty string
    D[0,0] = 0
    for i in range(1, n + 1):
        D[i,0] = D[i-1,0] + 1
    for j in range(1, m + 1):
        D[0,j] = D[0,j-1] + 1

#    Recurrence relation
    for i in range(1, n + 1):
        for j in range(1, m + 1):
            deletion = D[i - 1, j] + 1 # Computing for recursive insertion and cost
            substitution = D[i - 1, j - 1] # Computing for recursive substitution and cost
            insertion = D[i, j - 1] + 1 # Computing for recursive deletion and cost

#             Checking to see if there is actual match before computing substitution
            if source[i - 1] != target[j - 1]:
                substitution += 1

            D[i,j] = min(deletion,substitution,deletion)

#    Termination
    return D[n,m]

if __name__ == "__main__":
    source  = "Intention"
    target = ""

    # Function to run Min Edit
    MED = MinEditDistance(source,target)
    print("The Minimum Distance from ",source," to ",target, "is: ",MED)
