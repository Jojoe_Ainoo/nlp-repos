# Emmanuel Jojoe Ainoo (MY NAIVE BAYES CLASSIFIER and LOGISTICS REGRESSION CLASSFIER)

import sys
import sklearn
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix

# sentences1 = ["Mason really loves food","Hannah loves food too","The whale is food"]
sentences = ["How many teams participated in the first world cup?", "Who holds the record for top scorer in a single World Cup?",
            "Should I sell my car myself or trade it in?", "How important is car maintenance?",
            "How much does AWS SAM cost to use", "Which languages does AWS SAM support"]


vectorizer = CountVectorizer()
features = vectorizer.fit_transform(sentences)
features_nd = features.toarray()

print(features_nd)
#
